import fiftyone as fo
import fiftyone.zoo as foz 
dataset = foz.load_zoo_dataset("mnist", split="test") 
session = fo.launch_app(dataset, port=8000, address='0.0.0.0', remote=True) 
session.wait()