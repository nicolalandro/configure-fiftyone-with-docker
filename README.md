# Configure fiftyone with docker
This repo contains an example of Dockerfile with fiftyone.

## RUN
* Basic srcit
```
docker-compose up
# got o localhost:8000
```
* other scripts
```
docker-compose run fiftyone python main_with_file.py
# got o localhost:8000
```
* Interacrive use
```
docker-compose run fiftyone /bin/bash
```

# References
* python
* docker, docker compose
* [fiftyone](https://voxel51.com/blog/automatically-set-up-a-new-ml-project-pain-free/): read and visualize datasets